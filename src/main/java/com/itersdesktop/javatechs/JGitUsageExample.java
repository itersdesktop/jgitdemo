package com.itersdesktop.javatechs;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;

public class JGitUsageExample {
    public static void main(String[] args) throws IOException, IllegalStateException, GitAPIException {
        File localPath = File.createTempFile("JGitTestRepository", "");
        System.out.println(localPath.getAbsolutePath());
        // delete repository before running this
        Files.delete(localPath.toPath());
        JGitUsageExample.example3(localPath);
    }

    private static void example1(File localPath) throws IOException, IllegalStateException, GitAPIException {
        // create the git repository with init
        try (Git git = Git.init().setDirectory(localPath).call()) {
            System.out.println("Created repository: " + git.getRepository().getDirectory());
            File myFile = new File(git.getRepository().getDirectory().getParent(), "testfile");
            if (!myFile.createNewFile()) {
                throw new IOException("Could not create file " + myFile);
            }

            // run the add-call
            git.add().addFilepattern("testfile").call();

            git.commit().setMessage("Initial commit").call();
            System.out.println("Committed file " + myFile + " to repository at " + git.getRepository().getDirectory());
            // Create a few branches for testing
            for (int i = 0; i < 10; i++) {
                git.checkout().setCreateBranch(true).setName("new-branch" + i).call();
            }
            // List all branches
            List<Ref> call = git.branchList().call();
            for (Ref ref : call) {
                System.out.println("Branch: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
            }

            // Create a few new files
            for (int i = 0; i < 10; i++) {
                File f = new File(git.getRepository().getDirectory().getParent(), "testfile" + i);
                f.createNewFile();
                if (i % 2 == 0) {
                    git.add().addFilepattern("testfile" + i).call();
                }
            }

            Status status = git.status().call();

            Set<String> added = status.getAdded();
            for (String add : added) {
                System.out.println("Added: " + add);
            }
            Set<String> uncommittedChanges = status.getUncommittedChanges();
            for (String uncommitted : uncommittedChanges) {
                System.out.println("Uncommitted: " + uncommitted);
            }

            Set<String> untracked = status.getUntracked();
            for (String untrack : untracked) {
                System.out.println("Untracked: " + untrack);
            }

            // Find the head for the repository
            ObjectId lastCommitId = git.getRepository().resolve(Constants.HEAD);
            System.out.println("Head points to the following commit :" + lastCommitId.getName());
        }
    }

    private static void example2(File localPath) throws IOException, IllegalStateException, GitAPIException {
        // create the git repository with init
        try (Git git = Git.init().setDirectory(localPath).call()) {
            System.out.println("Created repository: " + git.getRepository().getDirectory());
            File myFile = new File(git.getRepository().getDirectory().getParent(), "testfile");
            if (!myFile.createNewFile()) {
                throw new IOException("Could not create file " + myFile);
            }

            // run the add-call
            git.add().addFilepattern("testfile").call();
            git.commit().setMessage("Initial commit").call();
            System.out.println("Committed file " + myFile + " to repository at " + git.getRepository().getDirectory());

            Status status = git.status().call();

            Set<String> added = status.getAdded();
            for (String add : added) {
                System.out.println("Added: " + add);
            }
            Set<String> uncommittedChanges = status.getUncommittedChanges();
            for (String uncommitted : uncommittedChanges) {
                System.out.println("Uncommitted: " + uncommitted);
            }

            Set<String> untracked = status.getUntracked();
            for (String untrack : untracked) {
                System.out.println("Untracked: " + untrack);
            }

            // Find the head for the repository
            ObjectId lastCommitId = git.getRepository().resolve(Constants.HEAD);
            System.out.println("Head points to the following commit :" + lastCommitId.getName());
        }
    }

    private static void example3(File localPath) throws IOException, IllegalStateException, GitAPIException {
        try (Git git = Git.init().setDirectory(localPath).call()) {
            String gitDir = git.getRepository().getDirectory().getParent();
            System.out.println("Created repository: " + gitDir);
            File myFile = new File(gitDir, "testfile");
            if (!myFile.createNewFile()) {
                throw new IOException("Could not create file " + myFile);
            }

            // run the add-call
            git.add().addFilepattern("testfile").call();
            git.commit().setMessage("Initial commit").call();
            /*FileRepositoryBuilder builder = new FileRepositoryBuilder();
            Repository repo = builder.setGitDir(localPath).
                readEnvironment().
                findGitDir().
                setMustExist(true).
                build();
            System.out.println(repo);
            System.out.println(repo.getRepositoryState());*/
            // Uncomment the following line if we want to access an existing repository
            // localPath: File localPath = new File("/path/to/repo/.git")
            // Git git = Git.open(localPath);

            // create and add README.md
            File readme = new File(gitDir, "README.md");
            JGitUsageExample.appendText(readme, "*** Introduction ***");
            git.add().addFilepattern("README.md").call();
            git.commit().setMessage("Upload README.md file").setAmend(false).call();

            Status status = git.status().call();
            Set<String> added = status.getAdded();
            for (String add : added) {
                System.out.println("Added: " + add);
            }

            Set<String> uncommittedChanges = status.getUncommittedChanges();
            for (String uncommitted : uncommittedChanges) {
                System.out.println("Uncommitted: " + uncommitted);
            }

            // Update file and commit
            JGitUsageExample.appendText(myFile, "This commit aims to demonstrate how to use jgit to amend a commit");
            git.add().addFilepattern("testfile").call();
            git.commit().setMessage("Update the content of testfile").setAmend(false).call();

            // Update file and amend the recent commit
            JGitUsageExample.appendText(myFile, "Remember to setAmend(true)");
            git.add().addFilepattern("testfile").call();
            git.commit().setMessage("Update the content of testfile").setAmend(true).call();

            // Find the head for the repository
            ObjectId lastCommitId = git.getRepository().resolve(Constants.HEAD);
            System.out.println("Head points to the following commit :" + lastCommitId.getName());
        }
    }

    private static void appendText(File file, String text) throws IOException {
        FileWriter fr = new FileWriter(file, true);
        fr.write(text);
        fr.close();
    }
}
